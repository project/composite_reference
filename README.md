# Composite Reference

The Composite Reference module allows users to mark entity reference fields as composite.

Composite reference fields ensure that referenced entities get deleted when the referencing entity is deleted.

The deletion is prevented if the referenced entity is referenced by another entity but typically you should
ensure you only use this capability for entities that can be referenced only once.

## Usage

The module works with both Entity Reference and Entity Reference Revisions field types.

### Field configs

For configurable (bundle) fields, edit the field configuration and check the box to mark the reference as composite. The rest is taken care of.

### Base fields

For base fields, simply set a custom field setting on the entity reference or entity reference revision field definition, like so:

```
$fields['my_field'] = BaseFieldDefinition::create('entity_reference')
  ->setLabel(t('My reference'))
  ->setSettings([
    'target_type' => 'node',
    'composite_reference' => TRUE,
  ]);
```

If the base field gets overridden, this setting will get exported to the third party setting and it will continue to work.

## Contributing

Please read [the full documentation](https://github.com/openeuropa/openeuropa) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the available versions, see the [tags on this repository](https://github.com/openeuropa/composite_reference/tags).
